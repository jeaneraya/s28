// CRUS Operations:
/*
	1. Create - insert
	2. Read - find
	3. Update - update
	4. Destroy - delete
*/

//Insert Method () - Create documents in our DB

/*
	Syntax:
		Insert One Document:
			db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB",
			});


		Insert Many Documents:
			db.collectionName.insertMany([
				{
					"fieldA": "valueA",
					"fieldB": "valueB",
				},
				{
					"fieldA": "valueA",
					"fieldB": "valueB",
				}
			])
*/

//upon execution on Robo 3T make sure not to execute your data several times as it will duplicate

db.users.insertOne({
	"firstName": "Jean Victoria",
	"lastName": "Eraya",
	"age": 27,
	"email": "jean@gmail.com",
	"company": "none"
});

db.users.insertMany([
	{
		"firstName": "Kaysha",
		"lastName": "Puerto",
		"age": 28,
		"email": "kaysha@gmail.com",
		"department": "none"
	},
	{
		"firstName": "Jayven",
		"lastName": "Narvaza",
		"age": 27,
		"email": "jayven@gmail.com",
		"department": "none"
	}
]);

db.courses.insertMany([
	{
		"name": "Javascript 101",
		"price": 5000,
		"description": "Introduction to Javascript",
		"isActive": true,
	},
	{
		"name": "HTML 101",
		"price": 2000,
		"description": "Introduction to HTML",
		"isActive": true,
	},
	{
		"name": "CSS 101",
		"price": 2500,
		"description": "Introduction to CSS",
		"isActive": true
	}
]);

// Find Document / Method - Read

/*
	Syntax:
		db.collectionName.find() - this will retrieve all the documents from our DB.

		db.collectionName.find({"criteria": "value"}) - this will retrieve all the documents that will match our criteria.

		db.collectionName.findOne({"criteria": value}) - this will return the first document in our collection that atch our criteria.

		db.collection.findOne({}) - this will return the first document in our collection
*/

db.users.find();

db.users.find({
	"firstName": "Jean Victoria"
});

db.users.find({
	"firstName": "Kaysha",
	"age": 21
});

// Update Documents / Method- Updates our documents in our collection

/*
	updateOne() - Updating the first matching document on our collection.

	Syntax:
		db.collectionName.updateOne({
			"criteria": "value"
		},
		{
			$set: {
				"fieldToBeUpdated": "updatedValue"
			}	
		})

	updateMany() - multiple; it updates all the documents that matches our criteria

	Syntax:
		db.collectionName.updateMany(
			{
				"criteria": "value"
			},
			{
			$set: {
				"fieldToBeUpdated": "updatedValue"
			}	
		})
*/

db.users.insertOne({
	"firstName": "Trisha",
	"lastName": "Parks",
	"age": 22,
	"email": "trisha@gmail.com",
	"department": "none"
});

// Updating One Document

db.users.updateOne({
		"firstName": "Trisha"
	},
	{
		$set: {
			"firstName": "Trisha Jane",
			"lastName": "Trance",
			"age": 25,
			"email": "trishajane@gmail.com",
			"department": "Operations",
			"status": "active"
		}
	}
);

// Removing a field
db.users.updateOne(
{
	"firstName": "Trisha Jane",
},
{
	$unset:{
		"status": "active"
	}
}
);

// Updating Multiple Documents
db.users.updateMany(
		{
			"department": "none"
		},
		{
			$set: {
				"department": "HR"
			}
		}
	);

db.users.updateOne({},
		{
			$set: {
				"department": "Operations"
			}
		}
	);

db.courses.updateOne(
	{
		"name": "HTML 101"
	},
	{
		$set: {
			"isActive": false
		}
	}
);

db.courses.updateMany({},
{
	$set : {
		"enrollees": 10
	}
})

// Destroy or Delete Documents or Method - deleting documents from our collection

db.users.insertOne({
	"firstName": "Test"
})

// Deleting a single document
/*
	Syntax:
		db.collectionName.deleteOne({"criteria": "value"})
*/

db.users.deleteOne({
	"firstName": "test"
})

// Deleting multiple documents
/*
	Syntax:
		db.collectionName.deleteMany({"criteria": "value"})
*/

db.users.deleteMany({
	"department": "HR"
})

db.courses.deleteMany({});